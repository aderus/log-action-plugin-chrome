let db = null;
const request = indexedDB.open('CSBILogDB');

request.onerror = (event) => {
    console.log('Error opened DB');
};

request.onupgradeneeded = (event) => {
    db = event.target.result;

    db.createObjectStore("log", { keyPath: "key", autoIncrement: true });
};

request.onsuccess = (event) => {
    db = event.target.result;
};

chrome.runtime.onMessage.addListener((request,sender,sendResponse) => {
    let tr = db.transaction("log", "readwrite");
    let logs = tr.objectStore("log");    
    let rq = logs.add(request);
    let req = logs.openCursor();

    rq.onsuccess = function() {
        console.log("add");
    };

    req.onsuccess = function(event) {
        let cursor = event.target.result;
        if (cursor) {
            let key = cursor.primaryKey;
            let value = cursor.value;                        
            
            cursor.continue();

            let xhr = new XMLHttpRequest(); 
            let url = "http://localhost:5000/stream/WebElements"; 
            xhr.open("POST", url, true); 
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.onreadystatechange = function () { 
                if (xhr.readyState === 4 && xhr.status === 200) { 
                    console.log(this.responseText);
                } 
            }; 

            let data = JSON.stringify({ 'date': value.date, 'actionType': value.actionType, 'title': value.innerHTML, 'url': value.url.href, 'path': value.css }); 

            //let data = JSON.stringify({ "date": '05-05-2020', "actionType": 'click', "title": 'Заголовок', "url": 'http://mail.ru', "path": 'html>body' }); 
            xhr.send(data);
            console.log(data);

        }
        else {
            
        }
    };

        /*let xhr = new XMLHttpRequest(); 
        let url = "http://localhost:5000/stream/WebElements"; 
        xhr.open("POST", url, true); 
        xhr.setRequestHeader("Content-Type", "application/json"); 
        xhr.onreadystatechange = function () { 
            if (xhr.readyState === 4 && xhr.status === 200) { 
                console.log(this.responseText);
            } 
        }; 
        let data = this.newArr;
        //let data = JSON.stringify({ "date": '05-05-2020', "actionType": 'click', "title": 'Заголовок', "url": 'http://mail.ru', "path": 'html>body' }); 
        xhr.send(data);*/

        /*const xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://localhost:5000/stream/WebElements', true);       
        const params = this.newArr;
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(params));
        xhr.onload = () => {
            console.log(xhr.responseText);
        }*/

        /*let xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:5000/stream/WebElements', true);        
            xhr.onload = function () {            
                console.log(this.responseText);
            };
            xhr.send(this.newArr);   */  

    req.onerror = function(event) {
        console.err("error fetching data");
     };

    sendResponse( {status: 'ok'} );
    
});


