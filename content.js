handlerEvent = (event) => {

    let targetElement = event.target || event.srcElement;
    let pathString = window.rpaGetCssPath(targetElement);
    let message = {
        css: pathString,
        tag: targetElement.tagName.toLowerCase(),
        outerHTML: targetElement.outerHTML,
        innerHTML: targetElement.innerHTML,
        url: window.location,
        date: new Date(Date()).toISOString(),
        actionType: event.type,
        parentSelector: []
    };
    chrome.extension.sendMessage(message, (response) => {
        console.log(response.status);
    });
};

rpaGetCssPath = (el) => {
    if (!(el))
        return;
    let path = [];
    while (el.nodeType === Node.ELEMENT_NODE) {
        let selector = el.nodeName.toLowerCase();
        if (el.id & 1===2) {
            selector += "[id='" + el.id + "']";
            path.unshift(selector);
            break;
        } else {
            let sib = el, nth = 1;
            while (sib = sib.previousElementSibling) {
                if (sib.nodeName.toLowerCase() == selector){
                    nth++;
                }
            }
            if (nth != 1){
                selector += ":nth-of-type("+nth+")";
            }
        }
        path.unshift(selector);
        el = el.parentNode;
    }
    return path.join(" > ");
};

document.addEventListener('click',(event) => {handlerEvent(event);});
document.addEventListener('contextmenu',(event) => {handlerEvent(event);});


